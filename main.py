import random,tkinter
from dessins import *
from grap import *

class drag():
    def __init__(s):
        c.delete("all")
        g.colorIt()
        s.restart()
        s.dico={}
        for elem in g:
            res=c.create_oval(elem.x-10,elem.y-10,elem.x+10,elem.y+10,fill=elem.coul,tags=('toma',))
            s.dico[res]=elem
            for ng in elem.ng:
                c.create_line(elem.x,elem.y,ng.x,ng.y,tags=("LINEEEE",))
    def restart(s,e=None):
        s.obj,s.x,s.y=None,0,0
                
    def start(s,event):
        s.obj=c.find_closest(event.x,event.y)[0]
        s.x=event.x
        s.y=event.y
    
    def depl(s,event):
        dx=event.x-s.x
        dy=event.y-s.y
        c.move(s.obj,dx,dy)
        sommet=s.dico[s.obj]
        c.delete("LINEEEE")
        s.x=event.x
        s.y=event.y
        sommet.x,sommet.y=s.x,s.y
        c.create_text(s.x,s.y-25,text=str(sommet),tags=("LINEEEE",))
        s.update(True)
        
    def update(this,prob=False):
        for elem in g:
            if prob:
                for ng in elem.dicpond:
                    c.create_line(elem.x,elem.y,ng.x,ng.y,tags=("LINEEEE",))
                    c.create_text((elem.x+ng.x)//2,(elem.y+ng.y)//2,text=str(elem.dicpond[ng]),fill="red",font="consolas 10",tags=("LINEEEE",))
            else:
                for ng in elem.ng:
                    c.create_line(elem.x,elem.y,ng.x,ng.y,tags=("LINEEEE",))



g=Graph(random_graph(10))
f=tkinter.Tk()
f.title("Y'a robbi")
c=tkinter.Canvas(f)
c.config(width=500,height=500)

c.pack()
drag=drag()
drag.update(False)

c.tag_bind("toma","<ButtonPress>",drag.start)
c.tag_bind("toma","<ButtonRelease>",drag.restart)
c.tag_bind("toma","<B1-Motion>",drag.depl)


