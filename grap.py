from colorsys import hsv_to_rgb
import time,random,math
class S():
    def __init__(this,name=None,dessin=None):
        this.name=name.upper() if name else ''.join(chr(random.randrange(65,90)) for i in range(3));
        this.dessin=dessin if dessin else name
        this.nbcoul=0
        this.coul="#%02x%02x%02x" % tuple([int(c) for c in hsv_to_rgb(this.nbcoul,0.5,255)]);
        this.x,this.y=random.randrange(10,490),random.randrange(10,490)
        this.ng=list();
        this.flag=0;
        this.p=[];
        this.dicpond={}
        
    def addmany(self,others:iter):
        for elem in others:
            self.add(elem);
            
    def add(self,other):
        if type(other) is type(self):
            self.ng.append(other);
            other.ng.append(self);
            v=random.randrange(1,10)
            self.dicpond[other]=v
            other.dicpond[self]=v
        else:
            self.add(self.__class__(str(other)));
    def addPonderated(self,other,val):
        if type(other) is type(self):
            self.ng.append(other);
            self.dicpond[other]=val
            other.ng.append(self)
            other.dicpond[self]=val

    def probTo(self,other):
        return self.dicpond[other]

    def parcours(self,n):
        liste=[self];
        visitsession=hash(time.time())
        self.flag=visitsession
        for k in range(n):
            nl=list();
            for yota in liste:
                nl+=[elem for elem in yota.ng if elem.flag!=visitsession]
                for elem in nl: elem.flag=vistsession
            liste=nl[:];
        return liste;
    @property
    def isWellColored(this):
        return all(elem.nbcoul!=this.nbcoul for elem in this.ng)
    
    def dist(self,other):
        liste=[self];
        visitsession=hash(time.time())
        self.flag=visitsession
        dist=0
        for elem in g:g.p=[]
        while liste:
            nl=list();
            for yota in liste:
                if yota==other:
                        return dist,yota.p+[yota]
                for elem in yota.ng:
                    if elem.flag-visitsession:
                        nl.append(elem)
                        elem.p=yota.p+[yota]
                for elem in nl:
                    elem.flag=visitsession
            liste=nl[:];
            dist+=1
        return -1;
    
                
    
    __repr__=lambda this:this.name

    
class Graph():
    def __init__(s,ls):
        s.ls=ls
    def __getitem__(this,arg):
        if type(arg)==int:
            return this.ls[arg]
        if type(arg)==str:
            arg=arg.upper()
            for elem in this.ls:
                if elem.name==arg:
                    return elem
            raise KeyError(arg)
        raise TypeError
    
    def doforall(this,func):
        for elem in this:func(elem)
        
    def dijkstra(this,s0,s1)->int:
        for elem in this:elem.distToHere=math.inf
        s0.distToHere=0
        flagsession=time.time()
        s=s0
        while s1.flag!=flagsession:
            s=min(s.ng,key=lambda e:e.distToHere)
            s.flag=flagsession
            for moussa in s.ng:
                if moussa.flag==flagsession:continue
                d=s.distToHere+s.probTo(moussa)
                if d<moussa.distToHere:
                    moussa.distToHere=d
        return s1.distToHere
    
    @property
    def wellColored(this):
        return all(elem.isWellColored for elem in this.ls)
    
    def colorIt(this):
        liste=[this[0]]
        maxnb=0
        for elem in this:elem.nbcoul=maxnb
        visitsession=hash(time.time())
        this[0].flag=visitsession
        while liste:
            nl=list();
            for yota in liste:
                nl+=[elem for elem in yota.ng if elem.flag!=visitsession]
                for elem in nl:
                    while not elem.isWellColored:
                        elem.nbcoul+=1
                        maxnb=max(maxnb,elem.nbcoul)
                    elem.flag=visitsession
            liste=nl[:]
            for elem in this:elem.coul="#%02x%02x%02x" % tuple([int(c) for c in hsv_to_rgb(elem.nbcoul/(maxnb+1),0.5,255)]);
        return maxnb

            
            
        
    __repr__=lambda self:self.ls.__repr__()
    __iter__=lambda self:self.ls.__iter__()
    __next__=lambda self:self.ls.__next__()
    __len__=lambda this:len(this.ls)

def random_graph(nb_points:int,nb_links=3):
    walah=[S() for i in range(nb_points)]
    for elem in walah[::random.randint(1,3)]:
        for i in range(nb_links):
            t=random.choice(walah)
            if t!=elem:t.add(elem)
    return walah

def random_graph_pond(nb_points:int,nb_links=3):
    walah=[S() for i in range(nb_points)]
    for elem in walah[::random.randint(1,3)]:
        for i in range(nb_links):
            random.choice(walah).addPonderated(elem,math.randrange(1,10))
    return walah

def complete_graph(nb_points):
    walah=[]
    for i in range(nb_points):
        walah.append(S())
        for elem in walah[:-1]:
            elem.add(walah[-1])
    return walah

def complete_graph_recur(nb_points,ls=[]):
    if nb_points==0:return ls
    new=S()
    for elem in ls:
        elem.add(new)
    ls.append(new)
    return complete_graph_recur(nb_points-1,ls)

if __name__=="__main__":
    g=Graph(random_graph(5))
